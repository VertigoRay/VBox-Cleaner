This script is ran when using GitLab runners and VirtualBox.

- The GitLab runner fails to delete or stop stale virtual machines, so this script checks which are stale and deletes them to prevent resource usage rampancy.
- Occasionally, orphaned runner VMs folders are left behind. We also clean those up.

Ref:

- https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/merge_requests/313
- https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/2067
