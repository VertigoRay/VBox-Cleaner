#!python

import virtualbox
import requests
import logging
import os
import signal
import shutil
import glob

from pprint import pprint

logging.basicConfig(filename='VBoxCleaner.log',level=logging.INFO,format='%(asctime)s %(message)s')
gitlab_token="abcd1234abcd1234abcd"

def gitlabRunnerCheck(projectID,OS):
    keepVM = False
    url = "https://gitlab.com/api/v3/projects/%s/builds?scope=running" % (projectID)
    header={'PRIVATE-TOKEN': gitlab_token}
    r = requests.get(url, headers=header)
    if r.status_code != 200:
        exit
    response = r.json()
    if len(response) > 0:
        for build in response:
            if OS in build['runner']['description']:
                keepVM = True
    else:
        return False
    if keepVM is False:
        return False

runnerVMs = []
vbox = virtualbox.VirtualBox()
for vm in vbox.machines:
    if "-runner-" in vm.name:
        runnerVMs.append(vm)

if len(runnerVMs) > 0:
    for vm in runnerVMs:
        nameSplit=vm.name.split("-")
        osName=nameSplit[0]
        projectID=nameSplit[4]
        if gitlabRunnerCheck(projectID,osName) is False:
            logging.info("Deleting: %s" % (vm.name))
            pid = vm.session_pid
            if pid < 4294967295:
                os.kill(pid, signal.SIGTERM)
            vm.remove(delete=True)
else:
    logging.info("Only %s runner VMs in total. Will not reap." % (len(runnerVMs)))

for d in glob.iglob(os.path.expanduser('~/VirtualBox VMs/*-runner-*')):
    is_orphaned = True

    for vm in runnerVMs:
        if os.path.basename(d) == vm.name:
            is_orphaned = False
            break

    if is_orphaned:
        logging.info("\tDeleting orphaned runner directory: %s" % d)
        shutil.rmtree(d)
